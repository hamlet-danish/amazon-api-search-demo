(function($){
    $(document).ready(function () {
        var admin_url = '/wp-admin/admin-ajax.php';
        var cronSwitch = $('input#amazon_api_cron_enabled');

        //init
        setTimeout(equalizeColumns, 100);
        $('input#amazon_search_exclude').tagsInput();
        new Switchery(cronSwitch[0]);
        $("#modal").iziModal();

        //display new schedule modal
        $('button.add-schedule').click(function(e){
            e.preventDefault();
            $('#modal').iziModal('open');
        });

        $(document).on('closing', '#modal', function (e) {
            clearValidationMessages();
            $('input#schedule_name').val('');
            $('input#schedule_interval').val('');
        });

        //submit new schedule form
        $('button#create-schedule').click(function(e){
            e.preventDefault();
            clearValidationMessages();
            var data = {
                'name': $('input#schedule_name').val(),
                'interval': $('input#schedule_interval').val(),
                'action': 'create_custom_schedule'
            };

            if(!data.name || data.name.length < 3){
                showValidationMessage(false, 'Schedule name is required and should be more than 2 characters long.');
                return;
            }

            if(!data.interval || data.interval < 60){
                showValidationMessage(false, 'Schedule interval is required and can\'t be less than 60 sec.');
                return;
            }

            $.post(admin_url, data, function(resp){
                if(resp.status){
                    buildCustomIntervalsTable(resp.intervals);

                    $('input#schedule_name').val('');
                    $('input#schedule_interval').val('');
                    iziToast.success({
                        title: 'Success!',
                        message: resp.message,
                    });
                    $('#modal').iziModal('close');
                }else{
                    showValidationMessage(false, resp.message);
                }
            }, "json");
        });

        //Delete schedule
        $('div.amazon-search-form-wrapper').on('click', 'button.delete-schedule', function(e){
            var intervalID = $(this).data('key');
            iziToast.question({
                timeout: 20000,
                close: false,
                overlay: true,
                toastOnce: true,
                id: 'question',
                zindex: 999,
                title: 'Warning',
                message: 'All associated ASCLs will be deactivated. Are you sure?',
                position: 'center',
                buttons: [
                    ['<button><b>YES</b></button>', function (instance, toast) {
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        var data = {
                            'id': intervalID,
                            'action': 'delete_custom_schedule'
                        };

                        if(!data.id) return;

                        $.post(admin_url, data, function(resp){
                            if(resp.status){
                                iziToast.success({
                                    title: 'Success!',
                                    message: 'Schedule deleted! All attached ASCLs are deactivated.'
                                });
                                buildCustomIntervalsTable(resp.intervals);
                            }else{
                                iziToast.error({
                                    title: 'Error',
                                    message: 'Error occurred, try again later.'
                                });
                            }
                        }, "json");
                    }, true],
                    ['<button>NO</button>', function (instance, toast) {
                        instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                    }]
                ]
            });
        });

        cronSwitch.change(function(){
            var enabled = $(this).prop('checked');
            var data = {
                'action': 'amazon_cron_deactivate'
            };

            if(enabled){
                data.action = 'amazon_cron_activate';
            }

            $.post(admin_url, data, function(resp){
                if(resp.status){
                    iziToast.success({
                        title: 'Success!',
                        message: 'Cron processing ' + ((enabled) ? 'activated!' : 'deactivated!')
                    });

                    updateNextRun(resp.next_run);
                }else{
                    iziToast.error({
                        title: 'Error',
                        message: 'Error occurred, try again later.'
                    });
                }
            }, "json");
        });

        $(window).resize(equalizeColumns);

        //process log table
        $('#ascl-log-table').DataTable({
            ajax: ajaxurl + '?action=get_ascl_log_data',
            processing: true,
            serverSide: true,
            responsive: true,
            order: [[0, "desc"]],
            columns: [
                {data: 'id'},
                {data: 'ascl_id'},
                {data: 'started_at'},
                {data: 'finished_at'},
                {data: 'total_runs'},
                {data: 'total_items_fetched'},
                {data: 'total_items_parsed'},
                {data: 'total_variations_parsed'},
                {data: 'errors_count'}
            ],
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        return '<a target="_blank" href="/wp-admin/post.php?action=edit&post='+ data +'">' + data + '</a>';
                    },
                    "targets": 1
                },{
                    "render": function ( data, type, row ) {
                        return moment.unix(data).format('DD/MM/YYYY HH:mm:ss');
                    },
                    "targets": 2
                },{
                    "render": function ( data, type, row ) {
                        return moment.unix(data).format('DD/MM/YYYY HH:mm:ss');
                    },
                    "targets": 3
                }
            ]
        });

        function buildCustomIntervalsTable(intervals) {
            var html = '';
            if(!intervals){
                html += '<tr>' +
                    '<td class="empty" colspan="3">No custom schedules found.</td>' +
                '</tr>'
            }else{
                $.each(intervals, function(key, item){
                    html += '<tr>' +
                        '<td>' + item.display + '</td>' +
                        '<td>' + item.interval + '</td>' +
                        '<td class="text-right">' +
                            '<button class="button delete-schedule" data-key="'+ key +'">Delete</button>' +
                        '</td>'
                });
            }

            $('table.custom-schedules tbody').html(html);
        }

        function updateNextRun(time){
            var text = (time) ? time : 'N/A'
            $('table.cron-form td.next-run').text(text);
        }

        function clearValidationMessages(){
            var messageContainer = $('table.interval-form td.message-container');
            messageContainer.empty();
        }

        function showValidationMessage(status, message){
            var messageContainer = $('table.interval-form td.message-container');
            var msg = '<span class="message '+ (status ? "success" : "error") +'">' + message + '</span>';
            messageContainer.append(msg);
        }

        function equalizeColumns() {
            var boxes = $('div.box-wrapper.right, div.box-wrapper.left');
            boxes.css({
                'min-height': 0
            });

            var maxHeight = $('div.boxes-wrapper').outerHeight();

            if($('body').outerWidth() <= 752){
                boxes.css({
                    'min-height': 0
                });
            }else{
                boxes.css({
                    'min-height': maxHeight
                });
            }
        }
    });
})(jQuery);