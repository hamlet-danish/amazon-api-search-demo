<?php
include_once('settings.php');
include_once('client.php');
include_once('parser.php');
include_once('product-builder.php');

class AmazonApiCronWrapper extends AmazonApiSettings {

    public function __construct(){
        parent::__construct();

        add_action(self::CRON_HOOK, array($this, 'cronHandler'));
    }

    public function dropCronLock(){
        update_option(self::CRON_LOCK, '0');
    }

    public function cronHandler(){
        $lock = get_option(self::CRON_LOCK);
        $searchResults = false;

        $ascl = $this->getCleanAsclList();
        $currentAscl = $this->getFirstAsclInQueue($ascl);

        //break if lock is set
        if($lock){
            //check if not hang-up
            $lastRunID = get_option(self::LAST_ASCL_ID);
            if($lastRunID){
                $lastAsclEnd = get_post_meta($lastRunID, self::ASCL_LAST_RUN, true);
                $diff = time() - (int)$lastAsclEnd;
                if($diff >= 600){
                    //deactivate & mark ASCL
                    update_post_meta($currentAscl, 'active', 0);
                    update_post_meta($currentAscl, 'force_deactivated', 1);

                    //Disable lock and re-run
                    update_option(self::CRON_LOCK, '0');
                    $this->cronHandler();
                }
            }

            return false;
        }

        //set lock before processing
        update_option(self::CRON_LOCK, '1');
        try{
            if($currentAscl){
                $searchResults = $this->runSearch($currentAscl);
            }

            //unlock before exit
            update_option(self::CRON_LOCK, '0');
        } catch (Exception $e){
            //unlock on error
            update_option(self::CRON_LOCK, '0');
        }

        update_option(self::CRON_LOCK, '0');
        return $searchResults;
    }

    private function runSearch($ascl_id){
        $searchSettings = $this->getSearchSettings($ascl_id);
        $clientSettings = $this->getClientSettings();

        //Amazon API client is not configured, break
        if(!$clientSettings) return;
        $page = (int)get_post_meta($ascl_id,self::ASCL_PAGE, true);
        $old_ascl_id = get_option(self::LAST_ASCL_ID);
        $old_ascl_start = get_post_meta($old_ascl_id, self::LAST_ASCL_START, true);

        update_option(self::LAST_ASCL_ID, $ascl_id);

        if($page === 0){
            update_post_meta($ascl_id, self::LAST_ASCL_START, time());
        }

        //Setup client and run request
        $client = new AmazonApiClient($clientSettings);
        $xml = $client->fetchProducts($searchSettings);

        //Request got a non-200 response, skip to next iteration
        if(!$xml){
            update_option(self::LAST_ASCL_ID, $old_ascl_id);
            update_post_meta($old_ascl_id,self::LAST_ASCL_START, $old_ascl_start);

            //update errors count
            $errorsCount = (int)get_post_meta($ascl_id,self::ASCL_ERRORS_COUNT, true);
            if($errorsCount >= self::ASCL_ERRORS_LIMIT){
                //update last_run timestamp to skip current ascl
                update_post_meta($ascl_id, self::ASCL_LAST_RUN, time());
                update_post_meta($ascl_id, self::ASCL_ERRORS_COUNT, 0);
            }else{
                update_post_meta($ascl_id, self::ASCL_ERRORS_COUNT, ($errorsCount + 1));
            }
            return;
        }

        //Save XML to file for debug purposes
        $this->saveXMLDump($ascl_id, $xml);

        //Extract product details
        $parser = new AmazonApiXmlParser($xml);
        $products = $parser->extractProducts();
        $rrpList = $parser->getRRPList();
        $totalProducts = $parser->getTotalProductsCount();
        $totalPages = $parser->getTotalPages();
        $asclLimit = (int)get_post_meta($ascl_id,'limit', true);
        $fetched = 0;

        if($products){

            //save products
            new ProductBuilder($ascl_id, $products, $rrpList);

            //update ASCL counter
            $fetched = ((int)get_post_meta($ascl_id,self::ASCL_FETCHED, true)) + $parser->getTotalFetched();
            update_post_meta($ascl_id, self::ASCL_FETCHED, $fetched);

            //update ASCL counter
            $vars = ((int)get_post_meta($ascl_id,self::ASCL_VARS, true)) + count($products);
            update_post_meta($ascl_id, self::ASCL_VARS, $vars);

            //update ASCL page
            update_post_meta($ascl_id, self::ASCL_PAGE, ($page + 1));
        }

        if(empty($totalProducts) || !$products || ($fetched >= $asclLimit || !$asclLimit) || $page >= $totalPages){
            //reached end for current ASCL, set last run timestamp, clear params, save details to log table
            $data = array(
                'ascl_id' => $ascl_id,
                'started_at' => get_post_meta($ascl_id, self::LAST_ASCL_START, true),
                'finished_at' => time(),
                'total_runs' => get_post_meta($ascl_id, self::ASCL_RUN_COUNT, true),
                'total_items_fetched' => $totalProducts,
                'total_items_parsed' => get_post_meta($ascl_id, self::ASCL_FETCHED, true),
                'total_variations_parsed' => get_post_meta($ascl_id, self::ASCL_VARS, true),
                'errors_count' => get_post_meta($ascl_id, self::ASCL_ERRORS_COUNT, true)
            );
            $this->logRun($data);

            update_post_meta($ascl_id, self::ASCL_LAST_RUN, time());
            update_post_meta($ascl_id, self::ASCL_PAGE, 0);
            update_post_meta($ascl_id, self::ASCL_FETCHED, 0);
            update_post_meta($ascl_id, self::ASCL_ERRORS_COUNT, 0);
            update_post_meta($ascl_id, self::ASCL_RUN_COUNT, 0);
            update_post_meta($ascl_id, self::ASCL_VARS, 0);

            $this->deleteObsoleteProducts($ascl_id);
        }else{
            $runs = ((int)get_post_meta($ascl_id,self::ASCL_RUN_COUNT, true)) + 1;
            update_post_meta($ascl_id, self::ASCL_RUN_COUNT, $runs);
        }

        return $products;
    }

    private function deleteObsoleteProducts($ascl_id){
        $ascl_start = get_post_meta($ascl_id, self::LAST_ASCL_START, true);
        if(empty($ascl_start)){
            $ascl_start = (time() - 24 * 3600);
        }

        $directlyRelated = get_posts(array(
            'numberposts'	 => -1,
            'post_type'		 => 'product',
            'meta_query'     => array(
                'relation' => 'AND',
                array(
                    'key'	  => 'ascl_id',
                    'value'   => $ascl_id,
                    'compare' => '='
                ),
                array(
                    'key' => 'ascl_last_parsed',
                    'value'   => $ascl_start,
                    'compare' => '<'
                )
            )
        ));

        $complexRelated = get_posts(array(
            'numberposts'  => -1,
            'post_type'	   => 'product',
            'post__not_in' => wp_list_pluck($directlyRelated, 'ID'),
            'meta_query'     => array(
                'relation' => 'AND',
                array(
                    'key'	  => 'ascl_id',
                    'value'   => '%'.$ascl_id.'%',
                    'compare' => 'LIKE'
                ),
                array(
                    'key' => 'ascl_last_parsed',
                    'value'   => $ascl_start,
                    'compare' => '<'
                )
            )
        ));

        if(!empty($directlyRelated) && is_array($directlyRelated)){
            foreach ($directlyRelated as $direct) {
                $this->deleteProductWithImage($direct->ID);
            }
        }

        if(!empty($complexRelated) && is_array($complexRelated)){
            foreach ($complexRelated as $complex) {
                $ascl_id = json_decode(get_post_meta($complex->ID, 'ascl_id', true), true);

                $ascl_id = array_filter($ascl_id, function($item) use($ascl_id){
                    return !($item === $ascl_id);
                });

                if(count($ascl_id) === 1){
                    $ascl_id = $ascl_id[0];
                }

                update_post_meta($complex->ID, 'ascl_id', json_encode($ascl_id));
            }
        }
    }

    private function getClientSettings(){
        $client = array(
            'associate_tag' => get_option('amazon_search_associate_tag'),
            'access_key' => get_option('amazon_search_access_key'),
            'secret_key' => get_option('amazon_search_secret_key')
        );

        $client = array_filter($client, function($item){
            return !empty($item);
        });

        if(count($client) < 3){
            return false;
        }

        return $client;
    }

    private function getSearchSettings($ascl_id){
        $page = (int)get_post_meta($ascl_id, self::ASCL_PAGE, true);

        $settings = array(
            'search_node' => get_post_meta($ascl_id, 'search_node', true),
            'min_discount' => get_post_meta($ascl_id, 'min_discount', true),
            'min_price' => get_post_meta($ascl_id, 'min_price', true),
            'max_price' => get_post_meta($ascl_id, 'max_price', true),
            'limit' => get_post_meta($ascl_id, 'limit', true),
            'exclude' => get_option('amazon_search_exclude'),
            'page' => ($page + 1)
        );

        $brand = get_post_meta($ascl_id, 'brand', true);
        if(!empty($brand)){
            $list = get_post_meta($brand, 'brands', true);
            if(!empty($list)){
                $settings['brand'] = $list;
            }
        }

        return $settings;
    }

    private function getFirstAsclInQueue($list){
        if(empty($list)) return false;

        $maxDelay = max(array_column($list, 'delay'));
        $currentAscl = false;
        $i = 0;

        while(!$currentAscl && $i <= count($list)){
            if($list[$i]['delay'] === $maxDelay){
                $currentAscl = $list[$i]['id'];
                break;
            }
            $i++;
        }

        return $currentAscl;
    }

    private function getCleanAsclList(){
        $ascl = get_posts(array(
            'numberposts'	=> -1,
            'post_type'		=> 'ascl',
            'meta_key'		=> 'active',
            'meta_value'	=> '1'
        ));

        $clean = array();
        foreach (wp_list_pluck((array)$ascl, 'ID') as $item) {
            $intervalKey = get_post_meta($item, 'update_interval', true);
            $intervals = wp_get_schedules();

            //No such interval, no need to proceed
            if(!array_key_exists($intervalKey, $intervals)) continue;

            $int = (int)$intervals[$intervalKey]['interval'];
            $last_update = (int)get_post_meta($item, self::ASCL_LAST_RUN, true);

            //No interval time, break
            if(!$int) continue;

            //Too early for this ascl
            if($last_update && (time() - $last_update) < $int) continue;

            $clean[] = array(
                'id' => $item,
                'last_update' => $last_update,
                'interval' => $int,
            );
        }

        return array_map(function($item){
            $item['delay'] = time() - (int)$item['last_update'] - (int)$item['interval'];
            return $item;
        }, $clean);
    }

    private function saveXMLDump($ascl_id, $xml){
        $dir = plugin_dir_path(__FILE__).'../xml-dumps/'.$ascl_id.'/';
        if(!is_dir($dir)){
            mkdir($dir);
        }

        if(count(scandir($dir)) >= 10){
            $files = glob( $dir.'*.*' );
            $exclude_files = array('.', '..');

            if (!in_array($files, $exclude_files)) {
                array_multisort(
                    array_map( 'filemtime', $files ),
                    SORT_NUMERIC,
                    SORT_ASC,
                    $files
                );
            }
            unlink($files[0]);
        }

        $name = date('m-d-Y H:i:s', time()).'_.xml';
        file_put_contents($dir.$name, $xml);
        update_option(self::LAST_ASCL_XML, $dir.$name);
    }

    private function logRun($data){
        global $wpdb;
        $wpdb->insert($wpdb->prefix.self::ASCL_LOG_TABLE, $data);
    }
}