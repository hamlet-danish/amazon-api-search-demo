<?php

class ProductBuilder {
    private $items = array();
    private $rrpList = array();
    private $updated = array();
    private $created = array();
    private $taxAttrs = array();
    private $debug = false;
    private $ascl_id = false;

    private $color = false;
    private $size = false;
    private $title = false;

    public function __construct($ascl_id, $items, $rrpList, $debug = false){
        $this->items = $items;
        $this->rrpList = $rrpList;
        $this->debug = $debug;
        $this->ascl_id = $ascl_id;

        if(!empty($items) && is_array($items)){
            if($debug){
                add_action('admin_init', array($this, 'createProducts'));
            }else{
                wp_set_current_user( 2 );
                $this->createProducts();
            }
        }
    }

    public function createProducts(){
        $sizeOverride = get_post_meta($this->ascl_id, 'size', true);
        if($sizeOverride === 'null'){
            $sizeOverride = false;
        }

        foreach ($this->items as $item) {
            $this->taxAttrs = array();
            $features = $this->buildContent(isset($item['features']) ? $item['features'] : false);
            $post = array(
                'post_author' => 1,
                'post_content' => $features,
                'post_excerpt' => $features,
                'post_status' => "publish",
                'post_title' => $this->cleanProductTitle($item['title']),
                'post_parent' => '',
                'post_type' => "product",
            );

            $this->title = $item['title'];

            //Check for list price and replace with max sibling's price
            //fix for products with no RRP / equal RRP and sale price
            $item = $this->fixListPrice($item, true);

            //Check for list price and replace with highest variation price
            //fix for products with no RRP / equal RRP and sale price
            $item = $this->fixListPrice($item, false);

            //Check for list price and calculate it manually in empty
            //fix for products with no RRP
            $item = $this->calculateListPrice($item);

            if(!$item['list_price'] || !$item['offer']['price'] || !$item['offer']['saved_percentage']){
                continue;
            }

            //check variation's discount against min discount
            $minDiscount = (int)get_post_meta($this->ascl_id, 'min_discount', true);
            $savedPercentage = (int)$item['offer']['saved_percentage'];
            if(!$savedPercentage || $savedPercentage < $minDiscount){
                continue;
            }

            //Skip if max rrp is set and item's rrp is bigger
            $maxRRP = (float)get_post_meta($this->ascl_id, 'local_max_rrp', true);
            if($maxRRP && $item['list_price'] && ((float)$item['list_price'] * 100) > $maxRRP ){
                continue;
            }

            //Skip if max price is set and item's price is bigger
            $maxPrice = (float)get_post_meta($this->ascl_id, 'local_max_price', true);
            if($maxPrice && $item['offer']['price'] && ((float)$item['offer']['price'] * 100) > $maxPrice ){
                continue;
            }

            //Skip if local_exclude_title is not empty and at least 1 local_exclude_title entry matches
            //product's title
            if(!$this->validateTitleLocalExclude($item['title'])){
                continue;
            }

            if($oldPost = $this->postExists($item['asin'])){
                $post_id = $oldPost->ID;
                $this->updated[$item['asin']] = $post_id;
                $post['ID'] = $post_id;


                unset($post['post_title']);
                $this->title = $oldPost->title;

                wp_update_post($post);
            }else{
                $post_id = wp_insert_post($post);
                $this->created[$item['asin']] = $post_id;
            }

            //post was not created, bail out
            if(!$post_id) continue;

            //Custom metas
            update_post_meta($post_id, 'asin', $item['asin']);
            update_post_meta($post_id, 'ascl_last_parsed', time());

            $old_ascl_id = json_decode(get_post_meta($post_id, 'ascl_id', true), true);
            if(is_array($old_ascl_id)){
                $old_ascl_id[] = $this->ascl_id;
                $insert = array_unique($old_ascl_id);
            }else if(empty($old_ascl_id)){
                $insert = $this->ascl_id;
            }else{
                $insert = array_unique(array((int)$old_ascl_id, (int)$this->ascl_id));
            }

            if(is_array($insert) && count($insert) === 1){
                $insert = $insert[0];
            }
            update_post_meta($post_id, 'ascl_id', json_encode($insert));

            //woocommerce metas
            update_post_meta( $post_id, '_visibility', 'visible' );
            update_post_meta( $post_id, '_stock_status', 'instock');
            update_post_meta( $post_id, '_downloadable', 'no');
            update_post_meta( $post_id, '_virtual', 'no');
            update_post_meta( $post_id, '_regular_price', $item['list_price'] );
            update_post_meta( $post_id, '_sale_price', ($item['offer']['price']) ? $item['offer']['price'] : $item['list_price'] );
            update_post_meta( $post_id, '_price', $item['list_price'] );
            update_post_meta( $post_id, 'aw_discount_percent', (int)$item['offer']['saved_percentage']);

            //gather categories for product
            $categories = array();
            $categories = array_merge($categories, $this->getAsclCategories());
            $genders = get_post_meta($this->ascl_id,'gender', true);

            if(!empty($genders) && !$oldPost){
                if(!is_array($genders)){
                    $genders = array( $genders );
                }

                $genders = array_map('intval', $genders);
                wp_set_object_terms( $post_id, $genders, 'pa_gender' ,true );

                $this->taxAttrs[] = array(
                    'name' => 'pa_gender',
                    'val' => $genders
                );
            }

            if(!empty($categories) && !$oldPost){
                wp_set_object_terms( $post_id, $categories, 'product_cat' ,true );
            }

            if(!empty($item['brand']) && !$oldPost){
                $this->setAttrTaxonomy($post_id, 'pwb-brand', $item['brand']);
            }

            if(!empty($item['attributes']) && !$oldPost){
                $this->setupAttributes($post_id, $item['attributes'], $sizeOverride);
            }

            if(!$oldPost){
                update_post_meta( $post_id, '_product_attributes', $this->buildAttributes($post_id, $sizeOverride));
            }

            if(!empty($item['images']) && is_array($item['images'])){
                $this->addProductImage($post_id, $item['images'], $oldPost);
            }

            do_action('search_filter_update_post_cache', $post_id);
        }
    }

    private function validateTitleLocalExclude($title){
        //Skip if local_exclude_title is not empty and at least 1 local_exclude_title entry matches
        //product's title
        $localExcludeTitle = get_post_meta($this->ascl_id, 'local_exclude_title', true);
        if(empty($localExcludeTitle)){
            return true;
        }

        $excludeList = explode('|', $localExcludeTitle);

        if(empty($excludeList) || !is_array($excludeList)){
            return true;
        }

        foreach ($excludeList as $exclude){
            $exclude = preg_quote(trim($exclude));
            if(preg_match('/\b'.$exclude.'\b/i', $title)){
                return false;
            }
        }

        return true;
    }

    private function cleanProductTitle($title){
        $title = str_replace("’", "'", $title);
        $t = $title;

        $title = preg_replace("/[^A-Za-z0-9 ,:\+\*\?\&\/\\\.\(\)-_@#\$%!\"';><]/", '', $title);
        return empty($title) ? $t : $title;
    }

    private function calculateListPrice($item){
        if(empty($item['list_price'])){
            $minDiscount = (int)get_post_meta($this->ascl_id, 'min_discount', true);
            $item['list_price'] = (float)$item['offer']['price'] / (1 - ($minDiscount / 100));

            $item['offer']['saved_amount'] = $item['list_price'] - (float)$item['offer']['price'];
            $item['offer']['saved_percentage'] = $minDiscount;
        }

        return $item;
    }

    private function fixListPrice($item, $useMaxSibling = false){
        if($item['list_price'] === $item['offer']['price'] || empty($item['list_price'])){
            if($useMaxSibling){
                $item['list_price'] = (!empty($this->rrpList[$item['parent_asin']]) && is_array($this->rrpList[$item['parent_asin']]))
                    ? max($this->rrpList[$item['parent_asin']]) : $item['max_list_price'];
            }else{
                $item['list_price'] = $item['max_list_price'];
            }

            //Recalculate saved_amount & saved_percentage
            $rrp = (float)$item['list_price'];
            $price = (float)$item['offer']['price'];
            if($rrp && $price && $rrp > $price){
                $item['offer']['saved_amount'] = $rrp - $price;

                $item['offer']['saved_percentage'] = floor((($rrp - $price) / $rrp) * 100);
            }
        }

        return $item;
    }

    private function setupAttributes($post_id, $attributes, $sizeOverride = false){
        foreach ($attributes as $key => $val) {
            $type = 'pa_'.strtolower($key);
            if($type === 'pa_color'){
                $type = 'pa_colour';
                $this->color = $val;
            }

            if($type === 'pa_size'){
                if($sizeOverride){
                    $term = get_term((int)$sizeOverride, $type);
                    if(!empty($term) && !is_wp_error($term)){
                        $this->size = $term->name;
                        wp_set_object_terms( $post_id, array( $term->term_id ), $type );

                        $this->taxAttrs[] = array(
                            'name' => 'pa_size',
                            'val' => array( $term->term_id )
                        );

                        continue;
                    }
                }else{
                    $this->size = $val;
                }
            }

            $this->setAttrTaxonomy($post_id, $type, $val);
        }
    }

    private function setAttrTaxonomy($post_id, $taxonomy, $value){
        $categories = get_term_by('name', $value, $taxonomy, ARRAY_A);

        $clean = array();
        if(isset($categories['term_id']) && !empty($categories['term_id'])){
            $clean[] = $categories['term_id'];
        }else{
            if($id = wp_insert_term($value, $taxonomy)){
                if(!is_wp_error($id)){
                    $clean[] = $id['term_id'];
                }
            }
        }

        if(!empty($clean)){
            $this->taxAttrs[] = array(
                'name' => $taxonomy,
                'val' => $clean
            );

            wp_set_object_terms( $post_id, $clean, $taxonomy );
        }
    }

    private function getAsclCategories(){
        $categories = wp_get_object_terms($this->ascl_id, 'product_cat');
        $clean = array();

        if(!empty($categories) && is_array($categories)){
            foreach ($categories as $category) {
                $clean[] = $category->term_id;
            }
        }
        return $clean;
    }

    private function buildContent($features){
        $content = '';

        if(is_array($features) && !empty($features)){
            foreach ($features as $feature) {
                $content .= '<li>'.$feature.'</li>';
            }

            if(!empty($content)){
                $content = '<ul>'.$content.'</ul>';
            }
        }

        return $content;
    }

    private function buildFileName($ext){
        $name = false;
        $title = false;
        if(empty($this->title) || empty($ext)){
            return false;
        }

        $name = $this->title;

        if(!empty($this->color)){
            $name .= '-'.$this->color;
        }

        if(!empty($this->size)){
            $name .= '-'.$this->size;
        }

        $title = str_replace('-', ' ', $name );
        $name .= '.'.$ext;

        $name = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $name);
        $name = mb_ereg_replace("([\.]{2,})", '', $name);
        $name = str_replace(' ', '-', $name);

        return compact('title', 'name');
    }

    private function addProductImage($post_id, $images, $oldPost = false){
        $img = reset($images);
        if(!empty($img)){
            // only need these if performing outside of admin environment
            include_once(ABSPATH . 'wp-admin/includes/media.php');
            include_once(ABSPATH . 'wp-admin/includes/file.php');
            include_once(ABSPATH . 'wp-admin/includes/image.php');
            include_once(ABSPATH . 'wp-includes/pluggable.php' );

            $fileName = false;
            $details = pathinfo($img);
            if(!empty($details)){
                $fileName = $this->buildFileName($details['extension']);
            }

            if(empty($fileName)){
                return;
            }

            //delete old featured image
            if($oldPost){
                $oldImageID = get_post_thumbnail_id($oldPost->ID);
                if($oldImageID){
                    delete_post_thumbnail($oldPost);
                    wp_delete_attachment($oldImageID, true);
                }
            }

            $file_array = array();
            $tmp = download_url( $img );
            $file_array['name'] = $fileName['name'];
            $file_array['tmp_name'] = $tmp;

            $id = media_handle_sideload( $file_array, $post_id, $fileName['title'] );

            if(!empty($id) && !is_wp_error($id)){
                $args = array(
                    'post_type' => 'attachment',
                    'posts_per_page' => -1,
                    'post_status' => 'any',
                    'post_parent' => $post_id
                );

                $attachments = get_posts($args);
                if(isset($attachments) && is_array($attachments)){
                    foreach($attachments as $attachment){
                        set_post_thumbnail($post_id, $attachment->ID);
                    }
                }
            }
        }
    }

    private function buildAttributes($post_id, $sizeOverride = false){
        $clean = array();
        foreach ($this->taxAttrs as $attr) {
            if($attr['name'] !== 'pwb-brand'){
                $clean[$attr['name']] = array(
                    'name' => $attr['name'],
                    'value' => $attr['val'],
                    'position' => 0,
                    'is_visible' => 1,
                    'is_variation' => 0,
                    'is_taxonomy' => 1,
                );
            }
        }

        if((!array_key_exists('pa_size', $clean) || empty($clean['pa_size']['value']))){
            if($sizeOverride){
                $termSize = get_term((int)$sizeOverride, 'pa_size');
            }else{
                $termSize = get_term_by('slug', "one-size", 'pa_size');
            }

            if(!empty($termSize) && !is_wp_error($termSize)){
                $clean['pa_size'] = array(
                    'name' => 'pa_size',
                    'value' => array( $termSize->term_id ),
                    'position' => 0,
                    'is_visible' => 1,
                    'is_variation' => 0,
                    'is_taxonomy' => 1,
                );
                wp_set_object_terms( $post_id, array( $termSize->term_id ), 'pa_size' );
            }
        }

        if(!array_key_exists('pa_colour', $clean) || empty($clean['pa_colour']['value'])){
            $termColor = get_term_by('name', 'Multicoloured', 'pa_colour');
            if(!empty($termColor) && !is_wp_error($termColor)){
                $clean['pa_colour'] = array(
                    'name' => 'pa_colour',
                    'value' => array( $termColor->term_id ),
                    'position' => 0,
                    'is_visible' => 1,
                    'is_variation' => 0,
                    'is_taxonomy' => 1,
                );
                wp_set_object_terms( $post_id, array( $termColor->term_id ), 'pa_colour' );
            }
        }

        $i = 1;
        foreach ($clean as $k => $v){
            $clean[$k]['position'] = $i;
            $i++;
        }

        return $clean;
    }

    private function postExists($asin){
        $products = get_posts(array(
            'numberposts'	=> -1,
            'post_type'		=> 'product',
            'post_status'   => 'publish',
            'meta_key'		=> 'asin',
            'meta_value'	=> $asin
        ));

        return isset($products[0]) && !empty($products[0]) ? $products[0] : false;
    }
}