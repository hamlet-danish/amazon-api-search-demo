<?php
class AmazonApiXmlParser{

    private $xml;
    private $items = false;
    private $ignoredAsins = array();
    private $multicolored = 'Multicoloured';
    private $colorsMap = array();
    private $brandsMap = array();
    private $sizesMap = array();
    private $rrpList = array();

    public function __construct($data){
        $this->xml = new SimpleXMLElement($data);
        $this->colorsMap = require_once('includes/colors-map.php');
        $this->sizesMap = require_once('includes/sizes-map.php');
        $this->brandsMap = require_once('includes/brands-map.php');

        if($this->xml && $this->xml->Items){
            $this->items = $this->xml->Items->Item;
        }

        $skip = get_option('amazon_search_exclude');
        if($skip){
            $this->ignoredAsins = explode(',', $skip);
        }
    }

    public function getTotalProductsCount(){
        $total = 0;
        if(isset($this->xml->Items->TotalResults)){
            $total = (int)$this->xml->Items->TotalResults->__toString();
        }
        return $total;
    }

    public function getTotalPages(){
        $total = 0;
        if(isset($this->xml->Items->TotalPages)){
            $total = (int)$this->xml->Items->TotalPages->__toString();
        }
        return $total;
    }

    public function getTotalFetched(){
        return is_array($this->items) ? count($this->items) : 0;
    }

    public function getRRPList(){
        return $this->rrpList;
    }

    public function extractProducts(){
        if(!$this->items) return false;

        $clean = array();
        foreach ($this->items as $item) {
            if($item->getName() !== 'Item') continue;

            if($variations = $this->getVariations($item)){
                $clean = array_merge($clean, $variations);
            }else{
                $itemDetails = $this->getItemDetails($item);
                if(!empty($itemDetails)){
                    $clean = array_merge($clean, $itemDetails);
                }
            }
        }

        return empty($clean) ? false : $clean;
    }

    private function getItemDetails($item){
        $data = array();
        $asin = $item->ASIN->__toString();
        $parentAsin = $asin;

        //skip ignored products
        if(in_array($asin, $this->ignoredAsins)){
            return $data;
        }

        $price = empty($item->ItemAttributes->ListPrice->Amount) ? 0 : $item->ItemAttributes->ListPrice->Amount->__toString();
        $data[$asin] = array(
            'asin' => $asin,
            'parent_asin' => $parentAsin,
            'title' => $item->ItemAttributes->Title->__toString(),
            'brand' => $this->normalizeBrand($item->ItemAttributes->Brand->__toString()),
            'list_price' => ($price) ? (float)$price / 100 : 0,
            'max_list_price' => ($price) ? (float)$price / 100 : 0,
        );

        if($item->ItemAttributes->Feature){
            foreach ($item->ItemAttributes->Feature as $feature){
                $data[$asin]['features'][] = $feature->__toString();
            }
        }

        if($item->LargeImage){
            $data[$asin]['images']['large'] = $item->LargeImage->URL->__toString();
        }

        if($item->MediumImage){
            $data[$asin]['images']['medium'] = $item->MediumImage->URL->__toString();
        }

        if($item->SmallImage){
            $data[$asin]['images']['small'] = $item->SmallImage->URL->__toString();
        }

        if($item->OfferSummary && !empty($item->OfferSummary->LowestNewPrice)){
            $offerPrice = $item->OfferSummary->LowestNewPrice->Amount->__toString();
            $data[$asin]['offer'] = array(
                'price' => ($offerPrice) ? (float)$offerPrice / 100 : 0,
                'saved_amount' => 0,
                'saved_percentage' => 0
            );

            //calculate saved amount and %
            $rrp = (float)$data[$asin]['list_price'];
            $price = $data[$asin]['offer']['price'];

            if($rrp && $price && $rrp > $price){
                $data[$asin]['offer']['saved_amount'] = $rrp - $price;
                $data[$asin]['offer']['saved_percentage'] = floor((($rrp - $price) / $rrp) * 100);
            }

            $this->rrpList[$data[$asin]['parent_asin']][] = $data[$asin]['list_price'];
        }else{
            $data = array();
        }

        return $data;
    }

    private function prepareAttribute($attr){
        $attr = preg_replace('/( \(.+\))+/', '', $attr);

        if(array_key_exists($attr, $this->sizesMap)){
            $attr = $this->sizesMap[$attr];
        }

        return $attr;
    }

    private function normalizeColor($attr){
        $normalized = false;
        foreach ($this->colorsMap as $mainColorName => $mainColor) {
            if(is_array($mainColor)){
                foreach($mainColor as $complexColor){
                    if(stripos($attr, $complexColor) !== false && !($complexColor === 'red' && stripos($attr, 'multicoloured') !== false)){
                        $normalized = $mainColorName;
                        break 2;
                    }
                }
            }else{
                if(stripos($attr, $mainColor) !== false && !($mainColor === 'red' && stripos($attr, 'multicoloured') !== false)){
                    $normalized = $mainColorName;
                    break;
                }
            }
        }

        return $normalized ? $normalized : $this->multicolored;
    }

    private function normalizeBrand($attr){
        $normalized = false;
        foreach ($this->brandsMap as $mainBrand => $list) {
            if(is_array($list)){
                foreach($list as $item){
                    if($this->strPosClean($attr, $item) !== false){
                        $normalized = $mainBrand;
                        break 2;
                    }
                }
            }else{
                if($this->strPosClean($attr, $list) !== false){
                    $normalized = $mainBrand;
                    break;
                }
            }
        }

        return $normalized ? $normalized : $attr;
    }

    private function strPosClean($haystack, $needle){
        $haystack = strtolower(str_replace("'", ' ', $haystack));
        $needle = strtolower(str_replace("'", ' ', $needle));

        return stripos($haystack, $needle);
    }

    private function getVariations($item){
        $vars = array();
        $maxPrice = !empty($item->VariationSummary->HighestPrice->Amount) ? $item->VariationSummary->HighestPrice->Amount->__toString() : '0';
        if(!empty($item->Variations->Item)){
            foreach ($item->Variations->Item as $var){
                $asin = $var->ASIN->__toString();
                $parentAsin = $var->ParentASIN->__toString();

                //skip ignored products
                if(in_array($asin, $this->ignoredAsins) || in_array($parentAsin, $this->ignoredAsins)){
                    continue;
                }

                $price = $var->ItemAttributes->ListPrice->Amount->__toString();
                $vars[$asin] = array(
                    'asin' => $asin,
                    'parent_asin' => $var->ParentASIN->__toString(),
                    'title' => $item->ItemAttributes->Title->__toString(),
                    'brand' => $this->normalizeBrand($var->ItemAttributes->Brand->__toString()),
                    'list_price' => ($price) ? (float)$price / 100 : 0,
                    'max_list_price' => ($maxPrice) ? (float)$maxPrice / 100 : 0,
                );

                if($var->ItemAttributes->Feature){
                    foreach ($var->ItemAttributes->Feature as $feature){
                        $vars[$asin]['features'][] = $feature->__toString();
                    }
                }

                if($var->LargeImage){
                    $vars[$asin]['images']['large'] = $var->LargeImage->URL->__toString();
                }

                if($var->MediumImage){
                    $vars[$asin]['images']['medium'] = $var->MediumImage->URL->__toString();
                }

                if($var->SmallImage){
                    $vars[$asin]['images']['small'] = $var->SmallImage->URL->__toString();
                }

                if($var->VariationAttributes){
                    foreach ($var->VariationAttributes->VariationAttribute as $varAttribute){
                        $attr = $this->prepareAttribute($varAttribute->Value->__toString());
                        if($varAttribute->Name->__toString() === 'Color'){
                            $attr = $this->normalizeColor($attr);
                        }

                        $vars[$asin]['attributes'][$varAttribute->Name->__toString()] = $attr;
                    }
                }

                if($var->Offers && !empty($var->Offers->Offer[0])){
                    $offer = $var->Offers->Offer[0]->OfferListing;
                    $price = $offer->Price->Amount->__toString();
                    $savedAmount = ($offer->AmountSaved) ? $offer->AmountSaved->Amount->__toString() : 0;
                    $vars[$asin]['offer'] = array(
                        'price' => ($price) ? (float)$price / 100 : 0,
                        'saved_amount' => ($savedAmount) ? (float)$savedAmount / 100 : 0,
                        'saved_percentage' => $offer->PercentageSaved->__toString()
                    );

                    $this->rrpList[$vars[$asin]['parent_asin']][] = $vars[$asin]['list_price'];
                }else{
                    unset($vars[$asin]);
                }
            }
        }

        return empty($vars) ? false : $vars;
    }
}