<?php


class AmazonApiSettings{

    private $schedules;
    const MAIN_INTERVAL_TIME = 60;
    const MAIN_INTERVAL_NAME = 'Amazon API main interval';
    const MAIN_INTERVAL_KEY = 'amazon_api_10_min_interval';
    const CRON_HOOK = 'amazon_api_cron_hook';

    const CRON_LOCK = 'amazon_api_cron_lock';
    const ASCL_LAST_RUN = 'amazon_api_cron_last_run';
    const LAST_ASCL_ID = 'amazon_api_last_ascl';
    const LAST_ASCL_XML = 'amazon_api_last_ascl_xml';
    const LAST_ASCL_START = 'amazon_api_last_ascl_start';
    const ASCL_FETCHED = 'ascl_last_run_total';
    const ASCL_VARS = 'ascl_variations_count';
    const ASCL_PAGE = 'ascl_last_page';
    const ASCL_ERRORS_COUNT = 'ascl_errors_count';
    const ASCL_LOG_TABLE = 'ascl_search_log';
    const ASCL_RUN_COUNT = 'ascl_run_count';
    const ASCL_ERRORS_LIMIT = 3;

    public function __construct()
    {
        add_action('init', array($this, 'registerBrandsListPostType'), 20);
        add_action('init', array($this, 'registerConfigListPostType'), 20);

        //Settings section
        add_action('admin_init', array($this, 'initSettingsPage'));
        add_action('admin_menu', array($this, 'addSettingsPage'));
        add_action('admin_enqueue_scripts', array($this, 'printScripts'));

        //Custom cron intervals
        add_filter( 'cron_schedules', array($this, 'addCustomIntervals'));

        //Ajax
        add_action('wp_ajax_create_custom_schedule', array($this, 'createSchedule'));
        add_action('wp_ajax_delete_custom_schedule', array($this, 'deleteSchedule'));
        add_action('wp_ajax_amazon_cron_activate', array($this, 'activateCron'));
        add_action('wp_ajax_amazon_cron_deactivate', array($this, 'deactivateCron'));
        add_action('wp_ajax_get_ascl_log_data', array($this, 'getLogData'));

        //Filter ASCL update_interval field options
        add_filter('acf/load_field/name=update_interval', array($this, 'populateIntervalOptions'));
        add_filter('acf/load_field/name=gender', array($this, 'populateGenderOptions'));
        add_filter('acf/load_field/name=size', array($this, 'populateSizeOptions'));
        add_filter('acf/load_field/name=brand', array($this, 'populateBrandOptions'));

        //Delete products on ASCL delete
        add_action('before_delete_post', array($this, 'handleAsclDelete'));

        $this->schedules = wp_get_schedules();
    }

    public function onActivate(){
        global $wpdb;

        $wpdb->query(
            'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix . self::ASCL_LOG_TABLE.'`(
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `ascl_id` INT(11) NOT NULL,
              `started_at` INT(11) NOT NULL,
              `finished_at` INT(11) NOT NULL,
              `total_runs` INT(11) NULL,
              `total_items_fetched` INT(11) NULL,
              `total_items_parsed` INT(11) NULL,
              `total_variations_parsed` INT(11) NULL,
              `errors_count` INT(11) NULL,
              
              UNIQUE KEY `id` (`id`)
            )'
        );
    }

    public function onDeactivate(){
        global $wpdb;
        $wpdb->query('DROP TABLE IF EXISTS `'.$wpdb->prefix . self::ASCL_LOG_TABLE.'`');
    }

    public function printScripts($hook){
        if($hook !== 'settings_page_amazon-search-settings') return;

        wp_enqueue_style( 'tags-input-styles', plugin_dir_url(__FILE__).'../assets/jquery.tagsinput.min.css' );
        wp_enqueue_style( 'modal-styles', plugin_dir_url(__FILE__).'../assets/iziModal/iziModal.min.css' );
        wp_enqueue_style( 'toast-styles', plugin_dir_url(__FILE__).'../assets/iziToast/iziToast.min.css' );
        wp_enqueue_style( 'switchery-styles', plugin_dir_url(__FILE__).'../assets/switchery/switchery.min.css' );
        wp_enqueue_style( 'data-tables-style', plugin_dir_url(__FILE__).'../assets/DataTables/datatables.min.css' );
        wp_enqueue_style( 'amazon-search-styles', plugin_dir_url(__FILE__).'../assets/amazon-search-styles.css', array('tags-input-styles', 'modal-styles', 'switchery-styles', 'toast-styles', 'data-tables-style') );

        wp_enqueue_script( 'tags-input-scripts', plugin_dir_url(__FILE__).'../assets/jquery.tagsinput.min.js', array('jquery') );
        wp_enqueue_script( 'modal-scripts', plugin_dir_url(__FILE__).'../assets/iziModal/iziModal.min.js', array('jquery') );
        wp_enqueue_script( 'toast-scripts', plugin_dir_url(__FILE__).'../assets/iziToast/iziToast.min.js', array('jquery') );
        wp_enqueue_script( 'switchery-scripts', plugin_dir_url(__FILE__).'../assets/switchery/switchery.min.js', array('jquery') );
        wp_enqueue_script( 'data_tables', plugin_dir_url(__FILE__).'../assets/DataTables/datatables.min.js', array('jquery') );
        wp_enqueue_script( 'moment', plugin_dir_url(__FILE__).'../assets/moment/moment.min.js', array('jquery') );
        wp_enqueue_script( 'amazon-search-scripts', plugin_dir_url(__FILE__).'../assets/amazon-search-scripts.js', array('tags-input-scripts', 'modal-scripts', 'switchery-scripts', 'toast-scripts', 'data_tables', 'moment') );
    }

    public function registerBrandsListPostType(){
        $labels = array(
            'name'               => _x( 'Brand List', 'post type general name' ),
            'singular_name'      => _x( 'Brand List', 'post type singular name' ),
            'menu_name'          => _x( 'Brand Lists', 'admin menu' ),
            'name_admin_bar'     => _x( 'Brand List', 'add new on admin bar' ),
            'add_new'            => _x( 'Add New', 'ascl' ),
            'add_new_item'       => __( 'Add New Brand List' ),
            'new_item'           => __( 'New Brand List' ),
            'edit_item'          => __( 'Edit Brand List' ),
            'view_item'          => __( 'View Brand List' ),
            'all_items'          => __( 'All Brand Lists' ),
            'search_items'       => __( 'Search Brand Lists' ),
            'parent_item_colon'  => __( 'Parent Brand List:' ),
            'not_found'          => __( 'No Brand Lists found.' ),
            'not_found_in_trash' => __( 'No Brand Lists found in Trash.' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Brands list to use on ASCL page.' ),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'brand_list' ),
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => 5,
            'supports'           => array('title'),
        );

        register_post_type('brand_list', $args);
        flush_rewrite_rules(true);
    }

    public function registerConfigListPostType(){
        $labels = array(
            'name'               => _x( 'ASCLs', 'post type general name' ),
            'singular_name'      => _x( 'ASCL', 'post type singular name' ),
            'menu_name'          => _x( 'ASCL', 'admin menu' ),
            'name_admin_bar'     => _x( 'Config List', 'add new on admin bar' ),
            'add_new'            => _x( 'Add New', 'ascl' ),
            'add_new_item'       => __( 'Add New ASCL' ),
            'new_item'           => __( 'New ASCL' ),
            'edit_item'          => __( 'Edit ASCL' ),
            'view_item'          => __( 'View ASCL' ),
            'all_items'          => __( 'All ASCLs' ),
            'search_items'       => __( 'Search ASCLs' ),
            'parent_item_colon'  => __( 'Parent ASCL:' ),
            'not_found'          => __( 'No ASCLs found.' ),
            'not_found_in_trash' => __( 'No ASCLs found in Trash.' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => __( 'Amazon Search Configuration List to setup search settings' ),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'ascl' ),
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => 5,
            'supports'           => array('title'),
            'taxonomies'         => array('product_cat')
        );

        register_post_type('ascl', $args);
        flush_rewrite_rules(true);
    }

    public function addSettingsPage(){
        add_options_page(
            'Amazon Search Settings',
            'Amazon Search Settings',
            'manage_options',
            'amazon-search-settings',
            array($this, 'buildSettingsPage')
        );
    }

    public function initSettingsPage(){
        add_settings_section(
            'amazon_search_settings_section',
            '',
            array($this, 'settingsPageCallback'),
            'amazon-search-settings'
        );

        add_settings_field(
            'amazon_search_associate_tag',
            'Amazon associate tag',
            array($this, 'buildAssociateTagInput'),
            'amazon-search-settings',
            'amazon_search_settings_section'
        );

        add_settings_field(
            'amazon_search_access_key',
            'Amazon access key',
            array($this, 'buildAccessKeyInput'),
            'amazon-search-settings',
            'amazon_search_settings_section'
        );

        add_settings_field(
            'amazon_search_secret_key',
            'Amazon secret access key',
            array($this, 'buildSecretKeyInput'),
            'amazon-search-settings',
            'amazon_search_settings_section'
        );

        add_settings_field(
            'amazon_search_exclude',
            'List of ASINs to exclude',
            array($this, 'buildExcludeInput'),
            'amazon-search-settings',
            'amazon_search_settings_section'
        );

        register_setting('amazon_search_settings_group', 'amazon_search_associate_tag');
        register_setting('amazon_search_settings_group', 'amazon_search_access_key');
        register_setting('amazon_search_settings_group', 'amazon_search_secret_key');
        register_setting('amazon_search_settings_group', 'amazon_search_exclude');
    }

    public function settingsPageCallback(){
        echo '<p>Setup Amazon API credentials and list of ignored ASINs.</p>';
    }

    public function buildAssociateTagInput(){
        echo '<input type="text" name="amazon_search_associate_tag" id="amazon_search_associate_tag" value="'.get_option("amazon_search_associate_tag").'"/>';
    }

    public function buildAccessKeyInput(){
        echo '<input type="text" name="amazon_search_access_key" id="amazon_search_access_key" value="'.get_option("amazon_search_access_key").'"/>';
    }

    public function buildSecretKeyInput(){
        echo '<input type="text" name="amazon_search_secret_key" id="amazon_search_secret_key" value="'.get_option("amazon_search_secret_key").'"/>';
    }

    public function buildExcludeInput(){
        echo '<input type="text" name="amazon_search_exclude" id="amazon_search_exclude" value="'.get_option("amazon_search_exclude").'"/>';
    }

    public function addCustomIntervals($schedules){
        $intervals = json_decode(get_option('ascl_custom_intervals'), true);
        if(!empty($intervals)){
            $schedules = array_merge($schedules, $intervals);
        }

        $schedules[self::MAIN_INTERVAL_KEY] = array(
            'interval' => self::MAIN_INTERVAL_TIME,
            'display' => self::MAIN_INTERVAL_NAME
        );

        return $schedules;
    }

    public function createSchedule(){
        $name = $_REQUEST['name'];
        $interval = $_REQUEST['interval'];
        $response = array(
            'status' => false,
            'message' => 'Schedule name or interval is not unique.'
        );

        if($this->validateSchedule($name, $interval)){
            $intervals = json_decode(get_option('ascl_custom_intervals'), true);
            $intervals['ascl_interval_'.(md5($name))] = [
                'display' => $name,
                'interval' => $interval
            ];
            update_option('ascl_custom_intervals', json_encode($intervals));

            $response['status'] = true;
            $response['message'] = 'Schedule interval saved.';
            $response['intervals'] = $intervals;
        }

        die(json_encode($response));
    }

    public function deleteSchedule(){
        $id = $_REQUEST['id'];
        $response = array(
            'status' => false,
        );

        $intervals = json_decode(get_option('ascl_custom_intervals'), true);
        if(array_key_exists($id, $intervals)){
            unset($intervals[$id]);
            $ascls = get_posts(array(
                'numberposts'	=> -1,
                'post_type'		=> 'ascl',
                'meta_key'		=> 'update_interval',
                'meta_value'	=> $id
            ));

            foreach ($ascls as $ascl) {
                update_post_meta($ascl->ID, 'active', '0');
            }

            update_option('ascl_custom_intervals', json_encode($intervals));

            $response['status'] = true;
            $response['intervals'] = empty($intervals) ? false : $intervals;
        }

        die(json_encode($response));
    }

    public function buildSettingsPage(){
        $standardSchedules = $this->getStandardSchedules();
        $customSchedules = $this->getCustomSchedules();
        $cronEnabled = wp_next_scheduled(self::CRON_HOOK);

        $cnt = wp_count_posts('ascl');
        $totalASCLs = $cnt->publish;
        $activeASCLs = count(get_posts(array(
            'numberposts'	=> -1,
            'post_type'		=> 'ascl',
            'meta_key'		=> 'active',
            'meta_value'	=> '1'
        )));

        $lastRunID = get_option(self::LAST_ASCL_ID);
        $lastAscl = false;
        if($lastRunID){
            $lastAscl = get_post($lastRunID);
            $lastAsclStart = get_post_meta($lastRunID, self::LAST_ASCL_START, true);
            $lastAsclEnd = get_post_meta($lastRunID, self::ASCL_LAST_RUN, true);
        }
        $lastRunXMLExists = get_option(self::LAST_ASCL_XML);

        return require_once(plugin_dir_path(__FILE__).'../templates/settings-page-template.php');
    }

    public function populateIntervalOptions($field){
        $field['choices'] = array();
        $intervals = array_merge($this->getStandardSchedules(), $this->getCustomSchedules());

        foreach ($intervals as $k => $v) {
            $field['choices'][ $k ] = $v['display'].' ('.$v['interval'].' sec.)';
        }

        return $field;
    }

    public function populateGenderOptions($field){
        $field['choices'] = array();
        $genders = get_terms(array(
            'taxonomy' => 'pa_gender',
            'hide_empty' => false,
        ));

        if(!is_wp_error($genders)){
            foreach ($genders as $gender) {
                $field['choices'][ $gender->term_id ] = $gender->name;
            }
        }

        return $field;
    }

    public function populateSizeOptions($field){
        $field['choices'] = array();
        $sizes = get_terms(array(
            'taxonomy' => 'pa_size',
            'hide_empty' => false,
        ));

        if(!is_wp_error($sizes)){
            foreach ($sizes as $size) {
                $field['choices'][ $size->term_id ] = $size->name;
            }
        }

        return $field;
    }

    public function populateBrandOptions($field){
        $field['choices'] = array();
        $brands = get_posts(array(
            'post_type'   => 'brand_list',
            'numberposts' => -1,
            'orderby'          => 'name',
            'order'            => 'ASC',
        ));

        if(!is_wp_error($brands)){
            foreach ($brands as $brand) {
                $list = get_post_meta($brand->ID, 'brands', true);
                if(!empty($list)){
                    $list = explode('|', $list);
                    $list = array_chunk($list, 3);
                    if(!empty($list[0])){
                        $list = implode('|', $list[0]) .  '...';
                    }else{
                        $list = '...';
                    }
                }else{
                    $list = '...';
                }
                $field['choices'][ $brand->ID ] = $brand->post_title.' ('.$list.')';
            }
        }

        return $field;
    }

    public function activateCron(){
        wp_schedule_event( time(), self::MAIN_INTERVAL_KEY, self::CRON_HOOK );
        die(json_encode(array('status' => true, 'next_run' =>  date('d/m/Y H:i:s', wp_next_scheduled(self::CRON_HOOK)))));
    }

    public function deactivateCron(){
        wp_clear_scheduled_hook( self::CRON_HOOK );
        die(json_encode(array('status' => true, 'next_run' => wp_next_scheduled(self::CRON_HOOK))));
    }

    private function getStandardSchedules(){
        return array_filter($this->schedules, function($key){
            return (
                    $key !== 'wp_1_wc_regenerate_images_cron_interval'
                    && $key !== 'wp_1_wc_updater_cron_interval'
                    && $key !== self::MAIN_INTERVAL_KEY
                    && strpos($key, 'ascl_interval_') === false
            );
        }, ARRAY_FILTER_USE_KEY);
    }

    private function getCustomSchedules(){
        return array_filter($this->schedules, function($key){
            return (strpos($key, 'ascl_interval_') !== false && $key !== self::MAIN_INTERVAL_KEY);
        }, ARRAY_FILTER_USE_KEY);
    }

    private function validateSchedule($name, $interval, $updating = false){
        //refresh schedules list
        $this->schedules = wp_get_schedules();

        $check = array_filter($this->schedules, function($val, $key) use($name, $interval){
            return ($val['display'] === $name) || ($val['interval'] === $interval);
        }, ARRAY_FILTER_USE_BOTH);

        return !count($check);
    }

    public function handleAsclDelete($post_id){
        $ascl = get_post($post_id);
        if(empty($ascl) || empty($ascl->post_type) || $ascl->post_type !== 'ascl'){
            return;
        }

        $directlyRelated = get_posts(array(
            'numberposts'	 => -1,
            'post_type'		 => 'product',
            'meta_key'		 => 'ascl_id',
            'meta_value_num' => $post_id,
            'meta_compare'   => '='
        ));

        $complexRelated = get_posts(array(
            'numberposts'  => -1,
            'post_type'	   => 'product',
            'post__not_in' => wp_list_pluck($directlyRelated, 'ID'),
            'meta_key'	   => 'ascl_id',
            'meta_value'   => '%'.$post_id.'%',
            'meta_compare' => 'LIKE'
        ));


        if(!empty($directlyRelated) && is_array($directlyRelated)){
            foreach ($directlyRelated as $direct) {
                $this->deleteProductWithImage($direct->ID);
            }
        }

        if(!empty($complexRelated) && is_array($complexRelated)){
            foreach ($complexRelated as $complex) {
                $ascl_id = json_decode(get_post_meta($complex->ID, 'ascl_id', true), true);

                $ascl_id = array_filter($ascl_id, function($item) use($ascl_id){
                    return !($item === $ascl_id);
                });

                if(count($ascl_id) === 1){
                    $ascl_id = $ascl_id[0];
                }

                update_post_meta($complex->ID, 'ascl_id', json_encode($ascl_id));
            }
        }
    }

    public function deleteProductWithImage($product_id){
        $oldImageID = get_post_thumbnail_id($product_id);
        if($oldImageID){
            delete_post_thumbnail($product_id);
            wp_delete_attachment($oldImageID, true);
        }

        wp_delete_post($product_id, true);
    }

    public function getLogData(){
        global $wpdb;

        $columns = $_GET['columns'];
        $count_filtered = false;

        $draw = $_GET['draw'];
        $offset = esc_sql($_GET['start']);
        $limit = esc_sql($_GET['length']);
        $search = esc_sql($_GET['search']['value']);
        $order_by = esc_sql($_GET['order'][0]['column']);
        $order = esc_sql($_GET['order'][0]['dir']);

        $query = 'SELECT * FROM `'.$wpdb->prefix.self::ASCL_LOG_TABLE.'`';
        if(!empty($search)){
            $where_clause = " WHERE `ascl_id` LIKE '{$search}%'";
            $query .= $where_clause;
            $count_filtered = $wpdb->get_var('SELECT COUNT(id) FROM `'.$wpdb->prefix.self::ASCL_LOG_TABLE.'`' . $where_clause);
        }

        $query .= " ORDER BY {$columns[$order_by]['data']} {$order}";
        $query .= " LIMIT {$limit} OFFSET {$offset}";

        $count_total = $wpdb->get_var('SELECT COUNT(id) FROM `'.$wpdb->prefix.self::ASCL_LOG_TABLE.'`');

        if($count_filtered === false){
            $count_filtered = $count_total;
        }

        $data = $wpdb->get_results($query);

        die(json_encode([
            "draw" => $draw,
            "recordsTotal" => $count_total,
            "recordsFiltered" => $count_filtered,
            "data" => $data
        ]));
    }
}