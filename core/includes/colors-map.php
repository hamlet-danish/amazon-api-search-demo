<?php
return array(

    "Black" => array(
        'black','midnight', 'ebony', 'jet', 'coal', 'dark', 'schwarz', 'noir', 'negro', 'zwart', 'nero'
    ),
    "Blue" => array(
        'blue', 'navy', 'indigo', 'sky', 'cornflower', 'ocean', 'sapphire', 'cobalt', 'marine', 'sea', 'blau',
        'bleu', 'azul', 'blauw', 'blu', 'royal', 'denim'
    ),
    "Brown" => array(
        'brown', 'chocolate', 'khaki', 'bronze', 'chestnut', 'coffee', 'copper', 'mahogany', 'sepia', 'tan',
        'wood', 'camouflage', 'camel', 'braun', 'marron', 'marr�n', 'bruin', 'bark', 'marrone', 'rust'
    ),
    "Cream" => array(
        'cream', 'beige', 'linen', 'moccasin', 'wheat', 'desert', 'sahne', 'creme', 'cr�me', 'crema',
        'room', 'crema', 'sandy','birch'
    ),
    "Gold" => array(
        'golden', 'gold', 'goud', 'oro'
    ),
    "Green" => array(
        'green', 'lime', 'olive', 'emerald', 'moss', 'pesto', 'forest', 'mint', 'army', 'spring', 'jade', 'gr�n',
        'grun', 'vert', 'verde', 'groen', 'leaf', 'north atlantic'
    ),
    "Grey" => array(
        'grey', 'gray', 'grau', 'carbon', 'carbone', 'slate', 'charcoal', 'graphite', 'gris', 'grijs', 'grigio', 'ash',
        'alloy', 'steel'
    ),
    "Orange" => array(
        'orange', 'tangerine', 'naranja', 'oranje', 'arancia', 'sunrise', 'mango'
    ),
    "Pink" => array(
        'pink', 'coral', 'lavender', 'orchid', 'peach', 'rosa', 'rose', 'rosado', 'roze'
    ),
    "Purple" => array(
        'purple', 'magenta', 'violet', 'plum', 'lila', 'p�rpura', 'purpura', 'purper', 'viola', 'elderberry'
    ),
    "Red" => array(
        'red', 'maroon', 'blood', 'ruby', 'cherry', 'scarlet', 'burgundy', 'rot', 'rouge', 'rojo', 'rood',
        'rosso', 'chilli', 'chili'
    ),
    "Silver" => array(
        'silver', 'platinum', 'metal', 'silber', 'argent', 'plata', 'zilver', 'argento'
    ),
    "Transparent" => array(
        'transparent', 'clear', 'transparente', 'turquesa', 'transparant', 'trasparente'
    ),
    "Turquoise" => array(
        'turquoise', 'cyan', 'teal', 'aqua', 't�rkis', 'turkis', 'turquesa', 'turkoois', 'turchese'
    ),
    "White" => array(
        'white', 'snow', 'ivory', 'pearl', 'wei�', 'weib', 'blanc', 'blanco', 'wit', 'bianca'
    ),
    "Yellow" => array(
        'yellow', 'mustard', 'amber', 'saffron', 'sun', 'corn', 'blonde', 'daffodil', 'canary', 'gelb',
        'jaune', 'amarillo', 'geel', 'giallo'
    ),
);