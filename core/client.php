<?php

class AmazonApiClient {
    protected $tag;
    protected $key;
    protected $secret;

    public $endpoint = "webservices.amazon.co.uk";
    public $uri = "/onca/xml";

    public function __construct($settings){
        $this->tag = $settings['associate_tag'];
        $this->key = $settings['access_key'];
        $this->secret = $settings['secret_key'];
    }

    public function fetchProducts($searchSettings){
        $pairs = $this->buildParams($searchSettings);

        $canonical_query_string = join("&", $pairs);
        $string_to_sign = "GET\n".$this->endpoint."\n".$this->uri."\n".$canonical_query_string;
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $this->secret, true));
        $request_url = 'http://'.$this->endpoint.$this->uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);
        return $this->makeRequest($request_url);
    }

    private function makeRequest($url){
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if($code !== 200){
            return false;
        }

        return $response;
    }

    private function buildParams($searchSettings){
        $params = array(
            "Service" => "AWSECommerceService",
            "Operation" => "ItemSearch",
            "AWSAccessKeyId" => $this->key,
            "AssociateTag" => $this->tag,
            "SearchIndex" => "Apparel",
            "ResponseGroup" => "Images,ItemAttributes,Variations,OfferSummary",
            "BrowseNode" => $searchSettings['search_node'],
            "MerchantId" => "Amazon",
            "Timestamp" => gmdate('Y-m-d\TH:i:s\Z'),
            "Availability" => "Available"
        );

        if(!empty($searchSettings['min_discount'])){
            $params["MinPercentageOff"] = $searchSettings['min_discount'];
        }

        if(!empty($searchSettings['page'])){
            $params["ItemPage"] = $searchSettings['page'];
        }

        if(!empty($searchSettings['brand'])){
            $params["Brand"] = $searchSettings['brand'];
        }

        if(!empty($searchSettings['max_price'])){
            $params["MaximumPrice"] = $searchSettings['max_price'];
        }

        if(!empty($searchSettings['min_price'])){
            $params["MinimumPrice"] = $searchSettings['min_price'];
        }

        ksort($params);
        $pairs = array();
        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
        }

        return $pairs;
    }
}