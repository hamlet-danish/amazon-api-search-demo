<div class="wrap">
    <h1>Amazon Search Settings</h1>
    <div class="amazon-search-form-wrapper">
        <div class="boxes-wrapper">
            <div class="box-wrapper left">
                <h2>Amazon search plugin settings</h2>
                <form method="post" action="options.php">
                    <?php settings_fields('amazon_search_settings_group') ?>
                    <?php do_settings_sections('amazon-search-settings') ?>
                    <?php submit_button(); ?>
                    <p class="submit">
                        <a href="?page=amazon-search-settings&manual_run_static=1" target="_blank" class="button">
                            Parse Static
                        </a>

                        <a href="?page=amazon-search-settings&manual_run=1" target="_blank" class="button">
                            Parse Amazon
                        </a>
                    </p>
                </form>
            </div><!--
            --><div class="box-wrapper right">
                <h2>General cron settings</h2>
                <p>Turn on/off all ASCLs</p>

                <div class="form-wrapper">
                    <form name="cronSettingsForm">
                        <table class="form-table cron-form">
                            <tbody>
                            <tr>
                                <th scope="row">Cron processing enabled</th>
                                <td class="text-right">
                                    <input type="checkbox" name="amazon_api_cron_enabled" value="1" <?php echo $cronEnabled ? 'checked="checked"' : ''; ?> id="amazon_api_cron_enabled">
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">Hard reset cron lock</th>
                                <td class="text-right">
                                    <a href="?page=amazon-search-settings&hard_reset_cron_lock=1" class="button">
                                        Reset lock
                                    </a>
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">ASCLs active/total</th>
                                <td class="text-right">
                                    <?php echo $activeASCLs.' / '.$totalASCLs; ?>
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">Next search run</th>
                                <td class="text-right next-run">
                                    <?php echo ($cronEnabled) ? date('d/m/Y H:i:s', $cronEnabled) : 'N/A'; ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="table-wrapper">
                    <table class="table last-run-stats">
                        <thead>
                        <tr>
                            <th colspan="3" class="table-title">
                                <span>Last search details</span>
                                <?php if($lastRunXMLExists): ?>
                                    <a href="?page=amazon-search-settings&download_latest_dump=1" class="button download-xml">Download XML</a>
                                <?php endif; ?>
                            </th>
                        </tr>
                        <tr>
                            <th>Detail</th>
                            <th>Value</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(empty($lastAscl)): ?>
                            <tr>
                                <td class="empty" colspan="2">No details available.</td>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <td>
                                    Name:
                                </td>
                                <td>
                                    <a target="_blank" href="/wp-admin/post.php?action=edit&post=<?php echo $lastAscl->ID; ?>">
                                        <?php echo $lastAscl->post_title; ?>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Duration (sec):
                                </td>
                                <td>
                                    <?php
                                        if(empty($lastAsclEnd) || empty($lastAsclStart)){
                                            echo "N/A";
                                        }else if($lastAsclEnd < $lastAsclStart){
                                            echo "Search in progress...";
                                        }else {
                                            echo $lastAsclEnd - $lastAsclStart;
                                        }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Date:
                                </td>
                                <td>
                                    <?php echo date('d/m/Y H:i:s', $lastAsclEnd); ?>
                                </td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="box-wrapper bottom">
            <h2>Schedule interval settings</h2>
            <p>Manage custom schedule intervals for ASCLs</p>

            <div class="table-wrapper schedules">
                <table class="table">
                    <thead>
                    <tr>
                        <th colspan="3" class="table-title">Built-in schedules</th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Interval (seconds)</th>
                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($standardSchedules as $k => $s): ?>
                        <tr>
                            <?php if($k !== 'wp_1_wc_regenerate_images_cron_interval' && $k !== 'wp_1_wc_updater_cron_interval'): ?>
                                <td><?php echo $s['display'] ?></td>
                                <td><?php echo $s['interval'] ?></td>
                                <td class="text-right">N/A</td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                    <?php if(empty($standardSchedules)): ?>
                        <tr>
                            <td class="empty" colspan="3">No standard schedules found.</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div><!--

            --><div class="table-wrapper schedules">
                <table class="table custom-schedules">
                    <thead>
                    <tr>
                        <th colspan="3" class="table-title">
                            <span>Custom schedules</span>
                            <button class="button add-schedule">Add</button>
                        </th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Interval (seconds)</th>
                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($customSchedules as $k => $s): ?>
                        <tr>
                            <td><?php echo $s['display'] ?></td>
                            <td><?php echo $s['interval'] ?></td>
                            <td class="text-right">
                                <button class="button delete-schedule" data-key="<?php echo $k ?>">Delete</button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if(empty($customSchedules)): ?>
                        <tr>
                            <td class="empty" colspan="3">No custom schedules found.</td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box-wrapper bottom">
            <h2>ASCL's search run log</h2>

            <div class="table-wrapper">
                <table class="table" id="ascl-log-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>ASCL ID</th>
                            <th>Start date</th>
                            <th>End date</th>
                            <th>Total runs</th>
                            <th>Total products found</th>
                            <th>Total products fetched</th>
                            <th>Total variations parsed</th>
                            <th>Non-200 responses</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal structure -->
<div id="modal" class="amazon-interval-modal" data-iziModal-fullscreen="false"
     data-iziModal-title="Create custom interval"
     data-iziModal-subtitle="&quot;Interval&quot; field should contain seconds between runs and should be unique."
>
    <div class="form-wrapper">
        <form name="scheduleIntervalForm">
            <table class="form-table interval-form">
                <tbody>
                <tr>
                    <th scope="row">Schedule name</th>
                    <td>
                        <input type="text" name="schedule_name" id="schedule_name" minlength="3">
                    </td>
                </tr>
                <tr>
                    <th scope="row">Schedule interval</th>
                    <td>
                        <input type="number" name="schedule_interval" id="schedule_interval" min="60">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="message-container"></td>
                </tr>
                </tbody>
            </table>

            <p class="submit">
                <button class="button button-primary" id="create-schedule">
                    Save
                </button>
            </p>
        </form>
    </div>
</div>
