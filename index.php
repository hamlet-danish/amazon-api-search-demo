<?php
/*
Plugin Name: Amazon Products API Scrapper
Description: Configurable scrapper for Amazon products.
Author: Cyril Komisarenko
Author URI: http://hamletdanish.info/
*/

include_once('core/settings.php');
include_once('core/cron.php');
register_activation_hook( __FILE__, array( 'AmazonApiSettings', 'onActivate' ) );
register_deactivation_hook( __FILE__, array( 'AmazonApiSettings', 'onDeactivate' ) );

//setup custom post type, add settings page, init cron wrapper
$cronWrapper = new AmazonApiCronWrapper();

//handle xml dump download
if(isset($_REQUEST['download_latest_dump'])){
    $file = get_option($cronWrapper::LAST_ASCL_XML);
    header("Content-Description: File Transfer");
    header("Content-Type: text/xml");
    header("Content-Disposition: attachment; filename='" . basename($file) . "'");

    readfile($file);
    exit();
}

//Debug request options
if(isset($_REQUEST['hard_reset_cron_lock'])){
    $cronWrapper->dropCronLock();
    header('Location: options-general.php?page=amazon-search-settings');
}

if(isset($_REQUEST['manual_run_static'])){
    include_once('core/parser.php');
    include_once('core/product-builder.php');
    $parser = new AmazonApiXmlParser(file_get_contents('test-response.xml', true));
    $data = $parser->extractProducts();
    $rrpList = $parser->getRRPList();

    $builder = new ProductBuilder(30689, $data, $rrpList, true);
}

if(isset($_REQUEST['manual_run'])){
    add_action('init', function() use ($cronWrapper){
        do_action($cronWrapper::CRON_HOOK);
    });
}